import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burger-builder-d8dc7.firebaseio.com/'
});

export default instance;